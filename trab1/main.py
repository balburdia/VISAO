from __future__ import division
import numpy as np
import getopt
import matplotlib.pyplot as plt
import sys

from faceid import FaceId
from faceid import ORLFaces
from faceid import YaleFaces

import operator


def main(argv):

    try:
        opts, args = getopt.getopt(argv, "d:n:k:mre")
    except getopt.GetoptError:
        print("-d <database> -n <number_eigenfaces> -k <number_knn>"
              "-e <number_efaces> -m -r")
        sys.exit(1)

    # default runtime variables
    database = 'yale'
    nEigen = 50
    recog = False
    nknn = 1

    for opt, arg in opts:
        if opt == '-d':
            database = arg
        if opt == '-e':
            nEigen = arg
        if opt == '-k':
            nknn = int(arg)
        if opt == '-r':
            recog = True

    if recog:
        db = None
        print('recognizing...')
        if database == 'yale':
            print("Yale Database")
            acuracies = dict()
            class_labels = ['centerlight', 'glasses', 'happy', 'leftlight', 'sad',
                            'noglasses', 'normal', 'rightlight', 'sleepy', 'surprised', 'wink']
            for exp_or_light in class_labels:
                if db:
                    del(db)
                db = YaleFaces(exp_or_light)
                db.get_images_and_labels()
                eFaces = db.eigenfaces(nEigen)
                correct = 0
                for i in range(len(db.test)):
                    to_recognize = db.test[i]
                    result, nearimage, neardist = db.recognition(
                        eFaces, to_recognize, nknn)
                    if (result == db.tsubjects[i]):
                        # print('Reconheceu corretamente o subject {0}'.format(result))
                        correct += 1
                    # else:
                        # print('Reconheceu incorretamente o subject {0} como sendo {1}'.format(
                        #     db.tsubjects[i], result))

                    # print('imagem mais proxima: {0}'.format(nearimage, neardist))
                    # ims1 = plt.subplot(1, 2, 1).imshow(to_recognize, cmap='Greys_r')
                    # ims2 = plt.subplot(1, 2, 2).imshow(
                    #     db.images[nearimage], cmap='Greys_r')
                    # plt.pause(3)
                # print('Acuracidade com label de test {}: {}%'.format(
                #       exp_or_light, (correct/len(db.test))*100))
                acuracies[exp_or_light] = (correct/len(db.test))
            minimum = min(acuracies.iteritems(), key=operator.itemgetter(1))[0]
            maximum = max(acuracies.iteritems(), key=operator.itemgetter(1))[0]
            print("Best acuracy with: {} - {}%\nWorst Acuracy with: {} - {}%".format(maximum, acuracies[maximum]*100, minimum, acuracies[minimum]*100))
        if database == 'orl':
            print("ORL Database")
            acuracies = []
            for label in range(10):
                if db:
                    del(db)
                db = ORLFaces(label)
                db.get_images_and_labels()
                eFaces = db.eigenfaces(nEigen)
                correct = 0
                for i in range(len(db.test)):
                    to_recognize = db.test[i]
                    result, nearimage, neardist = db.recognition(
                        eFaces, to_recognize, nknn)
                    if (result == db.tsubjects[i]):
                        # print('Reconheceu corretamente o subject {0}'.format(result))
                        correct += 1
                    # else:
                        # print('Reconheceu incorretamente o subject {0} como sendo {1}'.format(
                        #     db.tsubjects[i], result))
                acuracies.append(correct/len(db.test))
            print("Ten-fold Cross Validation:\nMean: {}\nStd: {}".format(np.mean(acuracies), np.std(acuracies)))

        else:
            print('Error: Database {} not found'.format(database))
            exit()


if __name__ == "__main__":
    main(sys.argv[1:])
