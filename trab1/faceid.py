# Import the required modules
import os
import numpy as np
import numpy.linalg as npla
from PIL import Image


class FaceId:

    DBPath = dict(yale='./yale_faces/',
                  orl='./orl_faces/')

    _path = ''  # virtual path

    def __init__(self, path=_path):
        self.path = path
        self.get_images_and_labels(self.path)

    def meanFace(self):
        # calcula a media das imagens
        meanF = np.average(np.asarray(self.images), axis=0)
        return meanF.reshape(-1, 1)

    def eigenfaces(self, nFaces):
        l = len(self.images)				# pega o numero de imagens e o tamanho delas
        h, w = self.images[0].shape

        imgs = np.ravel(np.asarray(self.images)).reshape(
            l, -1).T  # transforma cada imagem em um vetor
        # subtrai a imagem media de cada imagem da base
        subtracted = imgs - self.meanFace()

        # calcula a matriz de covariancia pela multiplicacao
        # calcula a matriz de covariancia
        mcv = np.dot(subtracted.T, subtracted)
        # calcula os eigenvalues e os eigenvectors
        evals, evects = npla.eig(mcv)
        evects = np.real(evects)			# transforma para reais

        eord = np.argsort(evals)
        eord[:] = eord[::-1]  # index of sorted eigenvalues

        eigenF = np.dot(subtracted, evects[:, eord[range(nFaces)]])
        # normalization of eigenface to unity
        for i in xrange(nFaces):
            eigenF[:, i] = eigenF[:, i] / npla.norm(eigenF[:, i])

        return eigenF

    def recognition(self, eFaces, image, knn):
        img = image.reshape(-1, 1)
        mface = self.meanFace()
        dif = img - mface
        proj = np.dot(eFaces.T, dif)

        dists = []
        for i in range(len(self.images)):
            img = self.images[i].reshape(-1, 1)
            dif = img - mface
            p = np.dot(eFaces.T, dif)
            dists.append(npla.norm(proj - p))

        dord = np.argsort(dists)
        neighbors = []
        for i in range(knn):
            neighbors.append(self.subjects[dord[i]])

        result = np.bincount(np.array(neighbors)).argmax()

        return result, dord[0], dists[dord[0]]


class ORLFaces(FaceId):

    _path = './orl_faces'

    def __init__(self, test_class):
        self.test_class = test_class

    def get_images_and_labels(self, path=_path):
        # images will contains face images
        self.images = []
        # subjets will contains the subject identification number assigned to
        # the image
        self.subjects = []

        # images to test
        self.test = []
        # subjects for test images
        self.tsubjects = []

        subjects_paths = [os.path.join(path, d) for d in os.listdir(
            path) if os.path.isdir(os.path.join(path, d))]
        for s, subject_paths in enumerate(subjects_paths, start=1):

            # Get the label of the image
            nbr = int(os.path.split(subject_paths)[
                      1].split(".")[0].replace("s", ""))
#			print 'sub: {0}--{1}'.format(subject_paths,nbr)

            subject_path = [os.path.join(subject_paths, f) for f in os.listdir(
                subject_paths) if f.endswith('.pgm') and os.path.isfile(os.path.join(subject_paths, f))]

            index_to_test = self.test_class

            for i in range(len(subject_path)):
                if (i == index_to_test):
                    image_path = subject_path[i]
                    # Read the image and convert to grayscale
                    image_pil = Image.open(image_path).convert('L')
                    # Convert the image format into numpy array
                    image = np.array(image_pil, 'uint8')  # normalization
                    self.test.append(image)
                    self.tsubjects.append(nbr)
                else:
                    image_path = subject_path[i]
                    # Read the image and convert to grayscale
                    image_pil = Image.open(image_path).convert('L')
                    # Convert the image format into numpy array
                    image = np.array(image_pil, 'uint8')  # normalization
                    self.images.append(image)
                    self.subjects.append(nbr)


class YaleFaces(FaceId):

    _path = './yale_faces'

    def __init__(self, test_class):
        self.class_labels = ['.centerlight', '.glasses', '.happy', '.leftlight', '.sad',
                             '.noglasses', '.normal', '.rightlight', '.sleepy', '.surprised', '.wink']
        self.class_labels.remove(".{}".format(test_class))
        self.test_class = test_class

    def get_images_and_labels(self, path=_path):

        # images will contains face images
        self.images = []
        # subjets will contains the subject identification number assigned to
        # the image
        self.subjects = []
        # classes
        self.classes = []

        # images to test
        self.test = []
        # subjects for test images
        self.tsubjects = []

        for c, class_label in enumerate(self.class_labels, start=1):
            image_paths = [os.path.join(path, f) for f in os.listdir(
                path) if f.endswith(class_label)]

            for image_path in image_paths:
                # Read the image and convert to grayscale
                image_pil = Image.open(image_path).convert('L')
                # Convert the image format into numpy array
                image = np.array(image_pil, 'uint8')  # normalization
                # Get the label of the image
                nbr = int(os.path.split(image_path)[1].split(
                    ".")[0].replace("subject", ""))

                self.images.append(image)
                self.subjects.append(nbr)
                self.classes.append(class_label)

        image_paths = [os.path.join(path, f)
                       for f in os.listdir(path) if f.endswith(self.test_class)]
        for image_path in image_paths:
            #				print 'Image: ' + image_path
            # Read the image and convert to grayscale
            image_pil = Image.open(image_path).convert('L')
            # Convert the image format into numpy array
            image = np.array(image_pil, 'uint8')  # normalization
            # Get the label of the image
            nbr = int(os.path.split(image_path)[1].split(
                ".")[0].replace("subject", ""))

            self.test.append(image)
            self.tsubjects.append(nbr)
